const { generate } = require("../services/token");
const task = require("../models/tasks");
const userModel = require("./../models/users");

async function login(req, res) {
    userModel.findOne({ email: req.body.email }).then(user => {
        if (!user) {
            return res.status(403).send("Invalid Email!");
        }

        if (!user.comparePassword(req.body.password)) {
            return res.status(403).send("Incorrect Password");
        }

        const token = generate({
            id: user._id,
            email: user.email,
            type: user.type
        });
        res.send({"token": token});
    }).catch(err => {
        console.log("error", err);
    })
}

async function logout(req, res) {
}

module.exports = {
    login,
    logout
}
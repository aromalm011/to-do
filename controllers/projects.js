const project = require("./../models/projects");
const projectModel = require("./../models/projects");

async function createProject(req, res) {
    const project = new projectModel({
        title: req.body.title,
        description: req.body.description,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        projectStatus: req.body.projectStatus,
        assigedTo: req.body.assigedTo,
        createdBy: req.body.createdBy
    });

    project.save().then(data => {
        console.log(data);
        res.send(data);
    }).catch(err => {
        console.log(err)
        res.send(err);
    });
}

async function viewProject(req, res) {
    projectModel.find().then(data => {
        res.send(data);
    }).catch(err => {
        res.send(err);
    });
}

function updateProject(req, res) {
    projectModel.findByIdAndUpdate({ _id: req.params.id }, {
        title: req.body.title,
        description: req.body.description,
        endDate: req.body.endDate,
        projectStatus: req.body.projectStatus,
        assigedTo: req.body.assigedTo
    }).then(data => {
        res.send(data);
    }).catch(err => {
        res.send(err);
    });
}

function projectStatus(req, res) {
    projectModel.findByIdAndUpdate({ _id: req.params.id }, {
        projectStatus: req.body.projectStatus
    }).then(data => {
        res.send(data);
    }).catch(err => {
        res.send(err);
    });
}

function deleteProject(req, res) {
    projectModel.deleteOne({ _id: req.body.id }).then(data => {
        res.send(data);
    }).catch(err => {
        res.send(err);
    });
}

module.exports = {
    viewProject,
    createProject,
    updateProject,
    deleteProject,
    projectStatus
}
// Updated

const task = require("./../models/tasks");
const taskModel = require("./../models/tasks");

async function createTask(req, res) {
    const task = new taskModel({
        title: req.body.title,
        description: req.body.description,
        priority: req.body.priority,
        assignTo: req.body.assignTo,
        createdBy: req.body.createdBy,
        taskStatus: req.body.taskStatus
    });

    task.save().then(data => {
        console.log(data);
        res.send(data);
    }).catch(err => {
        console.log(err)
        res.send(err);
    });
}

async function viewTask(req, res) {
    await taskModel.find({}).then(data => {
        res.send(data);
    }).catch(err => {
        res.send(err);
    });
}

function updateTask(req, res) {
    taskModel.findByIdAndUpdate({ _id: req.params.id }, {
        title: req.body.title,
        description: req.body.description,
        priority: req.body.priority,
        assignTo: req.body.assignTo,
        status: req.body.status
    }).then(data => {
        res.send(data);
    }).catch(err => {
        res.send(err);
    });
}

function changeStatus(req, res) {
    taskModel.findByIdAndUpdate({ _id: req.params.id }, {
        status: req.body.status
    }).then(data => {
        res.send(data);
    }).catch(err => {
        res.send(err);
    });
}

function deleteTask(req, res) {
    taskModel.deleteOne({ _id: req.body.id }).then(data => {
        res.send(data);
    }).catch(err => {
        res.send(err);
    });
}

module.exports = {
    viewTask,
    createTask,
    updateTask,
    deleteTask,
    changeStatus
}
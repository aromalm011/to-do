const registrationMail = require("../mail/registerationMail");
const userModel = require("./../models/users");


async function createUser(req, res) {
    const user = new userModel({
        email: req.body.email,
        fName: req.body.fName,
        lName: req.body.lName,
    });

    user.setPassword(req.body.password);

    user.save().then(data => {
        console.log(data);
        registrationMail().then(()=>{
        }).catch(err => {
            console.log(err)
        });
        res.status(201).send(data);
    }).catch(err => {
        console.log(err)
        res.send(err);
    });
}

async function getUser(req, res) {
    console.log("Res")
    userModel.find().then(data => {
        res.status(200).send(data);
    }).catch(err => {
        res.send(err);
    });
}

function updateUser(req, res) {
    const user =  {
        fName: req.body.fName,
        lName: req.body.lName,
        email: req.body.email,
        type: req.body.type
    };
    userModel.findByIdAndUpdate({ _id: req.params.id }, user).then(data => {
        res.status(200).send(data);
    }).catch(err => {
        res.send(err);
    });
}

function updatePasswordUser(req, res) {
    userModel.findById({ _id: req.params.id }).then(user => {
        if (!user.comparePassword(req.body.password)) {
            return res.status(401).send("Incorrect Password");
        }
        user.setPassword(req.body.newPassword);
        user.save().then(updatedUser => {
            res.status(200).send("Password updated successfully");
        });
    });
}

function deleteUser(req, res) {
    userModel.deleteOne({ _id: req.body.id }).then(data => {
        res.send(data);
    }).catch(err => {
        res.send(err);
    });
}

module.exports = {
    getUser,
    createUser,
    updateUser,
    deleteUser,
    updatePasswordUser
}
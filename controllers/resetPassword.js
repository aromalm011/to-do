const Token = require("../models/resetPassword");
const { generateSalt } = require("../services/encrypt");
const userModel = require("./../models/users");
const forgetPasswordMail = require("../mail/forgetPasswordMail");

async function generateLink(req, res) {
    try {
        const user = await userModel.findOne({ email: req.body.email });
        if (!user) {
            return res.status(403).send("Invalid Email!");
        }

       
        const salt = generateSalt();
        const newToken = new Token({
            email: req.body.email,
            salt: salt
        });

        await newToken.save();

        forgetPasswordMail(user.email, salt);
        return res.send("Email and Salt Stored");
    } catch (err) {
        console.log("error", err);
        res.status(500).send("An error occurred.");
    }
}

async function resetPassword(req, res) {
}

module.exports = {
    generateLink,
    resetPassword
}
const path = require("path");
const multer  = require('multer');
const imageModel = require("../models/images")

    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, 'uploads/avatar')
        },
        filename: function (req, file, cb) {
          const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
          cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname))
        }
      })

    const upload = multer({ dest: process.env.UPLOAD_PATH, storage: storage }).single('avatar');
    async function createAvatar(req, res, next) {
        upload(req, res, async function (err) {
          if (err instanceof multer.MulterError) {
            console.log(err);
            res.status(400).send('File upload error');
          } else if (err) {
            console.log(err);
            res.status(500).send('Server error');
          } else {
              const Images = new imageModel({
                imagePath: req.file.path,
                originalname: req.file.originalname,
                size: req.file.size,
                filename: req.file.filename
              })
              await Images.save();
              console.log(req.file);
              return res.json('File Upload Successfully');
            }
        });
      }

async function updateAvatar() {
}

module.exports = {
    createAvatar,
    updateAvatar
}
const { verify } = require("../services/token");

function adminAuthMiddleware(req, res, next) {
    const verified = verify(req.header("token"))
    
    if(verified.type !== 'admin') {
        return res.status(401).send("Unauthorized");
    }

    next();
}

module.exports = adminAuthMiddleware;
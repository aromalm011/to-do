const { verify } = require("../services/token");

function authMiddleware(req, res, next) {
    const verified = verify(req.header("token"))
    if (!verified) {
        return res.status(403).send("Auth Success");
    }
    next();
}

module.exports = authMiddleware;
const jwt = require("jsonwebtoken");

const key = process.env.JWT_KEY;

function generate(data) {
    console.log(data);
    return jwt.sign(data, key);
}

function verify(token) {
    return jwt.verify(token, key);
}

module.exports = {
    generate,
    verify
}
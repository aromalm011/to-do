const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
    host: process.env.NODEMAILER_HOST,
    port: process.env.NODEMAILER_PORT,
    // secure: false,
    auth: {
      user: process.env.NODEMAILER_USER,
      pass: process.env.NODEMAILER_PASSWORD,
    },
});

async function sendMail(from, to, sub, body) {
    const info = await transporter.sendMail({
        from: from,
        to: "aromalm011@gmail.com",
        subject: sub,
        html: body,
    });

    console.log("Message sent: %s", info.messageId);
}

module.exports = sendMail;

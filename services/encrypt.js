const crypto = require("crypto");

function generate(password, salt) {
    return crypto.pbkdf2Sync(password, salt, 1000, 64, "sha512").toString("hex");
}

function generateSalt() {
    return crypto.randomBytes(16).toString("hex");
}

function validate(password, oldPassword, salt) {
    const encryptedPassword = generate(password, salt);
    return encryptedPassword === oldPassword;
}

module.exports = {
    generate,
    generateSalt,
    validate
};

var express = require("express");
const dotenv = require("dotenv").config();

var app = express();

const mongoose = require("mongoose");
const dbConfig = require("./config/database");
const bodyparser = require('body-parser');

const userRoute = require("./routers/users");
const authRoute = require("./routers/auth")
const taskRoute = require("./routers/tasks");
const projectRoute = require("./routers/projects");
const passwordRoute = require("./routers/resetPassword");

const authMiddleware = require("./middleware/auth");

mongoose.connect(dbConfig.url).then(() => {
    console.log("Db connected to " + process.env.DB_HOST + process.env.DB_NAME);
}).catch(err => {
    console.log(err);
    process.exit();
});

app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());

app.use("/", express.static("public"));
app.use("/api/auth", authRoute);
app.use("/api/users", [authMiddleware], userRoute);
app.use("/api/tasks", [authMiddleware], taskRoute);
app.use("/api/projects", [authMiddleware], projectRoute);
app.use("/api/user/reset-password", passwordRoute);

app.listen(process.env.PORT, function (err) {
    console.log('App is Running ' + process.env.PORT);
});

const mongoose = require("mongoose");

const projectSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        new: true
    },
    description: {
        type: String,
    },
    startDate: {
        type: String,
        require: true
    },
    endDate: {
        type: String,
    },
    projectStatus: {
        type: String,
        default: "Work-in Progress"
    },
    createdBy:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true
      }
},
{
    timestamps: true
});

const project = mongoose.model("Project", projectSchema);

module.exports = project;
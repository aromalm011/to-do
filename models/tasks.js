const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        new: true
    },
    description: {
        type: String,
    },
    priority: {
        type: String,
    },
    assignTo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
      },
    taskStatus: {
        type: String,
        default: "To-Do"
    },
    createdBy:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true
      }
},
    {
        timestamps: true
    });

const task = mongoose.model("Task", taskSchema);

module.exports = task;
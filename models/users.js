const mongoose = require("mongoose");
const { generate, generateSalt, validate } = require("../services/encrypt");

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    fName: {
        type: String,
        required: true,
    },
    lName: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        enum: ['admin', 'user'],
        default: 'user'
    },
    salt: {
        type: String
    }
}, { timestamps: true });

userSchema.methods.setPassword = function(password) {
    this.salt = generateSalt();
    this.password = generate(password, this.salt);
};

userSchema.methods.comparePassword = function (password) {
    return validate(password, this.password, this.salt);
};

const User = mongoose.model("User", userSchema);

module.exports = User;
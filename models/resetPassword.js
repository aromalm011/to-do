const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const tokenSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  }
},
  { timestamps: true });

module.exports = Token = mongoose.model('token', tokenSchema);
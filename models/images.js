const mongoose = require("mongoose");

const imageSchema =  new mongoose.Schema ({
 type: {
    type: String,
    default: "Avatar"
 },
 type_id: {
    type: Number,
 },
 imagePath: {
    type: String,
 },
 filename: {
    type: String,
 },
 originalname: {
    type: String,
 }
},
{ timestamps: true });

module.exports = Images = mongoose.model('Images', imageSchema);
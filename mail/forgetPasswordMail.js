const sendMail = require('../services/mailer');
const ejs = require('ejs');
const path = require('path');

async function forgetMail(email, username, resetLink) {
    const subject = "Password Reset Instructions";
    const from = "aromalm011@gmail.com.com";
    const templatePath = path.join(__dirname, './templates/forgetPass.ejs');
    const to = "aromalm011@gmail.com" 
 
    const html = await ejs.renderFile(templatePath, { username, email, resetLink });

    const info = await sendMail(from, to, subject, html);
  

    // const link = `http://localhost:3000/api/user/reset-password/${user.email}/${resetToken}`;
    // sendEmail(user.email,"Password Reset Request",{name: user.name,link: link,},"./template/requestResetPassword.handlebars");

    return info;
}

module.exports = forgetMail;
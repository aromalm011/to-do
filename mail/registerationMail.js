const ejs = require('ejs');
const sendMail = require('../services/mailer');
const fs = require('fs').promises;
const path = require('path');

async function registrationMail(to, data) {
    const subject = "Registration Mail";
    const from = "aromalm011@gmail.com";

    try {
        const templatePath = path.join(__dirname, './templates/registerationMail.ejs');
        const templateContent = await fs.readFile(templatePath, 'utf8');

        const renderedTemplate = ejs.render(templateContent, data);

        const mailOptions = {
            from: `"Your Name" ${from}`,
            to: to,
            subject: subject,
            html: renderedTemplate,
        };
        console.log('Email sent successfully');
        sendMail(from, to, subject, renderedTemplate);
    } catch (error) {
        console.error('Error sending email: ' + error);
        throw error;
    }
}

module.exports = registrationMail;

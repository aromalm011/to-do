const express = require("express");
const router = express.Router();

const projectController = require("../controllers/projects");

router.get("/", projectController.viewProject);
router.post("/", projectController.createProject);
router.put("/:id", projectController.updateProject);
router.delete("/", projectController.deleteProject);
router.put("/status/:id", projectController.projectStatus);

module.exports = router
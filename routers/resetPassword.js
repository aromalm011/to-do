const express = require("express");
const router = express.Router();

const passwordController = require("../controllers/resetPassword");

router.post("/", passwordController.generateLink);
router.post("/:id/:token", passwordController.resetPassword);

module.exports = router;
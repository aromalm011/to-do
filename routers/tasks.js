const express = require("express");
const router = express.Router();

const taskController = require("../controllers/tasks");

router.get("/", taskController.viewTask);
router.post("/", taskController.createTask);
router.put("/:id", taskController.updateTask);
router.delete("/", taskController.deleteTask);
router.put("/status/:id", taskController.changeStatus);

module.exports = router
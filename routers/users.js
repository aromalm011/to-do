const express = require("express");

const router = express.Router();

const userController = require("../controllers/users");
const avatarController = require("../controllers/avatar");
const adminAuthMiddleware = require("../middleware/adminAuth");

router.get("/", userController.getUser);
router.post("/", [adminAuthMiddleware] ,userController.createUser);
router.put("/:id", userController.updateUser);
router.delete("/", [adminAuthMiddleware], userController.deleteUser);
router.put("/updatepassword/:id", userController.updatePasswordUser);
router.post("/avatar", avatarController.createAvatar);
router.put("/avatar", avatarController.updateAvatar);

module.exports = router;